## Write-Up for COVID's Communication Technology! (IoT Challenge 1)

Problem Description:
<pre>
We heard a rumor that COVID was leveraging that smart city's 'light' technology for communication. Find out in detail on the technology and what is being transmitted.

Please view this Document for download instructions.

This challenge:
- Is eligible for Awesome Write-ups Award
</pre>

Given
- [iot-challenge-1.logicdata](https://github.com/BrandonTang89/Code-Archive/blob/master/StacktheFlags2020/COVID's%20Communication%20Technology!/iot-challenge-1.logicdata)

### Stage 1: What's logicdata?
#### Opening the File
Seeing that the topic for this challenge is IoT and the challenge statement is about some light, I presumed that the .logicdata file was some sort of binary file for a smart light. As such, I searched google for various tools to open the file. As this was a CTF, I didn't want to waste time with tools that wouldn't work. Thus I referred to [this writeup](https://github.com/EmpireCTF/empirectf/blob/master/writeups/2018-06-19-SCTF/README.md) that I found which had a similar logicdata file.

I confirmed that it was similar by downloading his logicdata file and using the file bash command which returned a similar enough output as that of mine
<pre>
$ file transactions1.logicdata 
transactions1.logicdata: CLIPPER COFF executable C1 R1 not stripped - version 773 alignment trap enabled

$ file iot-challenge-1.logicdata 
iot-challenge-1.logicdata: CLIPPER COFF executable C1 R1 not stripped - version 610 -Csnc
</pre>

Refering to his write-up, I downloaded the [Saleae Logic](https://www.saleae.com/downloads/) software and opened the capture file (the logicdata file).

#### Interpreting the Signal
Loading it in, we see that the file consist of multiple pulses.
![Logic1](images/Logic1.png)

Zooming in, we see that these pulses are actually packets of data that are composed of smaller pulses.
![Logic2](images/Logic2.png)

Zooming in more, we see that the smaller pulses are comprised of alternative square waves.
![Logic3](images/Logic3.png)

From the first and second image, I infer that the signal consist of several packets of information each starting with a long starting pulse, followed by a bunch of shorter pulses with varying pauses between them but all of the same width. This likely corresponds to 0s and 1s where a lack of a pulse is a 0 and the presence of one is a 1. This the presence of the long starting pulse seems indicative of asynchronous data transfer.

### Stage 2: Generating the Binary Sequence

#### Pre-Coding Data Manipulation
Clearly, it was going to be impractical to manually decode the signal, thus we begin the difficult process of writing a script to automate the process. Luckily, the Logic software being used had the ability to export the signal to csv, which I did:
<pre>
Time[s], Channel 0
0.000000000000000, 0
4.818541075000000, 1
4.818548949999999, 0
4.818567325000000, 1
4.818575200000000, 0
4.818593575000000, 1
4.818601474999999, 0
4.818619825000000, 1
4.818627725000000, 0
4.818646100000000, 1
4.818653975000000, 0
4.818672350000000, 1
4.818680225000000, 0
4.818698599999999, 1
4.818706500000000, 0
</pre>
_Note: Only first few rows are shown_

From the third image, we can see that the alternating 1s and 0s correspond to the body of the pulses. Furthermore, as I scroll through the CSV file, it seems that times without a pulse are just represented as not having samples in the CSV file. As such, we can just take all the sample times as the times that the signal is part of a pulse.

Doing this and removing other useless features, we get the contents of iot1.csv
<pre>
4.818541075000000
4.818548949999999
4.818567325000000
4.818575200000000
4.818593575000000
4.818601474999999
4.818619825000000
4.818627725000000
4.818646100000000
4.818653975000000
4.818672350000000
4.818680225000000
4.818698599999999
4.818706500000000
4.818724850000000
4.818732750000000
4.818751124999999
</pre>
_Note: Only first few rows are shown_

#### Signal Processing in Python 3.8+
To attain the binary sequence, we use the following code
<pre>
# Importing Required Modules and Data
import matplotlib.pyplot as plt
import csv
import numpy as np

signal = []
with open("iot1.csv") as csvfile:
    reader = csv.reader(csvfile,delimiter=" ", quotechar="|")
    for row in reader:
        signal.append(float(row[0]))

# Step 1
## If the samples are too close together, they form part of the same pulse, so we only take the first sample of the pulse
discrete_signal = [0]
for x in signal:
    if discrete_signal[-1] + 0.0006 > x:
        continue
    
    discrete_signal.append(x)

# Step 2
## If the remaining samples are too close togther, they form the starting pulse, so we get rid of all except 1 of them
discrete2 = [0]
for i in range(len(discrete_signal)-1,0, -1):
    # print(i)
    if discrete_signal[i] - discrete_signal[i-1] < 0.001:
        continue

    discrete2.append(discrete_signal[i])

discrete2.reverse()

'''
# Code for Plotting Signal as Graph
print(discrete2)

ones = [1 for i in range(len(discrete2))]

plt.scatter(discrete2, ones)
plt.plot()
plt.show()
'''

# Step 3
## Using the sliding window technique, we fill a temporary array with samples within the same data packet
## Samples from different data packets are differentiated via the long time between samples of 2 adjacent data packets
packets = []
index = 0
temp_arr = []
while index < len(discrete2):
    temp_arr.append(discrete2[index])
    if index+1 >= len(discrete2): 
        packets.append(temp_arr[1:])
        temp_arr = []
        break
    if discrete2[index+1] - discrete2[index] > 0.5:
        packets.append(temp_arr[1:])
        temp_arr = []

    index+= 1

# Step 4
## The time between each sample is divided by the time between 2 consecutive samples (without a zero) to determine how many zeros are there
binaries = []
for packet in packets:
    binary = "1"
    # print(packet)
    for i in range(1, len(packet)):
        num_zero = round((packet[i] - packet[i-1])/0.001155275) -1
        # print(num_zero)
        binary += "0"*num_zero + "1"

    print(binary)
    binaries.append(binary)

'''
#Code for Plotting Packet as Graph 
ones = [1 for i in range(len(packet))]
plt.scatter(packet, ones)
plt.plot()
plt.show()
'''
</pre>

The code operates in 4 main steps
1. Converting the short data pulses into a single sample
2. Condensing the long starting pulses into a single sample
3. Separating the data packets into their own lists and removing the samples from the starting pulse
4. Replacing the time samples with 1s and 0s

With this code done, we get the following data sequence
<pre>
11111111101010101010101011111111111111111
11111111101010101010101011111111111111111
1111111110101010101010101101011101010110101101010101
11111111101010101010101011010101101011101010110111
1111111110101010101010101101011101101101011110101
111111111010101010101010110101101111110110101101
11111111101010101010101011010111101011010101110101
1111111110101010101010101101011101010110101010110101
11111111101010101010101011011111010110110110111
111111111010101010101010110101110101110110101010101
11111111101010101010101011011101110110110111011
111111111010101010101010110110101010101101110101011
11111111101010101010101011011110110110111110101
11111111101010101010101011011010101010111010111011
11111111101010101010101011101011111101111111
111111111010101010101010111010111111101111101
11111111101010101010101011011010101010110101010101101
11111111101010101010101011111111111111111
11111111101010101010101011111111111111111
1111111110101010101010101101011101010110101101010101
</pre>
_Note: Only first few rows are shown_

## Stage 3: Binary Data Interpretation
#### Clean the Data
From the output of the program, I notice that the data is periodic, so only the first few rows are important. Furthermore, The first 26 characters of each sequence are the same, so we can just get rid of those. This yields:
<pre>
111111111111111
111111111111111
01011101010110101101010101
010101101011101010110111
01011101101101011110101
0101101111110110101101
010111101011010101110101
01011101010110101010110101
011111010110110110111
0101110101110110101010101
011101110110110111011
0110101010101101110101011
011110110110111110101
011010101010111010111011
101011111101111111
1010111111101111101
011010101010110101010101101
</pre>

#### Intepretation
Now, I notice that the number of "1"s in each line is the same, and that there are no consecutive zeros. 

Looking at this list of [binary codes](https://en.wikipedia.org/wiki/List_of_binary_codes), it seems that most of the codes have a fixed length. Thus, I deduce that each "1" has to correspond to 1 bit, while the zeros determine what value the bit is.

As such, I inferred that each "01" actually corresponded to a "1" while a "1" (without a "0" in front) corresponds to a "0" in a modified binary sequence.

Applying this by replacing "01" with "2", "1" with "0", and "2" with "1" (in that order), we get:
<pre>
000000000000000
000000000000000
110011101101111
111011001110100
110010101100011
110100000101101
110001101110011
110011101111011
100001101010100
110011001011111
100100101010010
101111101001110
100010101000011
101111100110010
011000001000000
011000000100001
101111101111101
</pre>

From here, I notice that the length of each sequence is 15 bits, but then ascii characters are usually represented in just 7 bits. Observing further, I realise that the 8th bit of each sequence is 0. Thus I split each sequence into the first 7 and last 7 bits, discarding the 8th bit, getting:
<pre>
1100111
1101111
1110110
1110100
1100101
1100011
1101000
0101101
1100011
1110011
1100111
1111011
1000011
1010100
1100110
1011111
1001001
1010010
1011111
1001110
1000101
1000011
1011111
0110010
0110000
1000000
0110000
0100001
1011111
1111101
</pre>
_Note: First 4 of them are just 0000000, so I discarded them_

Converting this to ascii using [this converter](https://www.rapidtables.com/convert/number/binary-to-ascii.html), we get the flag: govtech-csg{CTf\_IR_NEC_20@0!}

_Note: After conversion, I actually got govtech-csg{CTf\_IR\_NEC\_20@0!\_}, but i inferred that the last \_ was not meant to be there, so I just removed it during submission._
