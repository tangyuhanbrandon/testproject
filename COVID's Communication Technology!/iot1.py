# Importing Required Modules and Data
import matplotlib.pyplot as plt
import csv
import numpy as np

signal = []
with open("iot1.csv") as csvfile:
    reader = csv.reader(csvfile,delimiter=" ", quotechar="|")
    for row in reader:
        signal.append(float(row[0]))

# Step 1
## If the samples are too close together, they form part of the same pulse, so we only take the first sample of the pulse
discrete_signal = [0]
for x in signal:
    if discrete_signal[-1] + 0.0006 > x:
        continue
    
    discrete_signal.append(x)

# Step 2
## If the remaining samples are too close togther, they form the starting pulse, so we get rid of all except 1 of them
discrete2 = [0]
for i in range(len(discrete_signal)-1,0, -1):
    # print(i)
    if discrete_signal[i] - discrete_signal[i-1] < 0.001:
        continue

    discrete2.append(discrete_signal[i])

discrete2.reverse()

'''
# Code for Plotting Signal as Graph
print(discrete2)

ones = [1 for i in range(len(discrete2))]

plt.scatter(discrete2, ones)
plt.plot()
plt.show()
'''

# Step 3
## Using the sliding window technique, we fill a temporary array with samples within the same data packet
## Samples from different data packets are differentiated via the long time between samples of 2 adjacent data packets
packets = []
index = 0
temp_arr = []
while index < len(discrete2):
    temp_arr.append(discrete2[index])
    if index+1 >= len(discrete2): 
        packets.append(temp_arr[1:])
        temp_arr = []
        break
    if discrete2[index+1] - discrete2[index] > 0.5:
        packets.append(temp_arr[1:])
        temp_arr = []

    index+= 1

# Step 4
## The time between each sample is divided by the time between 2 consecutive samples (without a zero) to determine how many zeros are there
binaries = []
for packet in packets:
    binary = "1"
    # print(packet)
    for i in range(1, len(packet)):
        num_zero = round((packet[i] - packet[i-1])/0.001155275) -1
        # print(num_zero)
        binary += "0"*num_zero + "1"

    print(binary)
    binaries.append(binary)

'''
#Code for Plotting Packet as Graph 
ones = [1 for i in range(len(packet))]
plt.scatter(packet, ones)
plt.plot()
plt.show()
'''